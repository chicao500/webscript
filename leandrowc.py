import requests					
from bs4 import BeautifulSoup

req = requests.get ('https://bitinfocharts.com/')

soup = BeautifulSoup (req.content, "html.parser")

selecao = soup.find_all('span')

for link in selecao:
	if 'BTC' in link.get_text('class') and 'class' not in link.get_text('class') and 'USD' in link.get_text('class'):
		print ("Bitcoin: ",link.get_text('class'))
	if 'LTC' in link.get_text('class') and 'class' not in link.get_text('class') and 'USD' in link.get_text('class'):
		print ("Litecoin: ",link.get_text('class'), '\n')
	if 'ETH' in link.get_text('class') and 'class' not in link.get_text('class') and 'USD' in link.get_text('class'):
		print ("Ethereum: ",link.get_text('class'))