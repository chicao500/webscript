import requests
from bs4 import BeautifulSoup
import re

candidato = input('Nome do candidato: ')

req = requests.get ('https://www.google.com.br/search?hl=pt-BR&biw=1311&bih=672&tbm=nws&ei=D16oW_nUH8GEwQTenL3YBw&q='+str(candidato)+'&oq='+str(candidato)+'&gs_l=psy-ab.3..0l10.7728.8047.0.8290.4.4.0.0.0.0.98.371.4.4.0....0...1c.1.64.psy-ab..0.4.368....0.VJCMqOqsp8Y')

soup = BeautifulSoup(req.content, "html.parser")

qtd =  soup.select('div div')

for link in qtd:
	if re.search('Aproximadamente', link.text, re.IGNORECASE): #busca de string
		print ('\n\n'+link.text+'\n\n')