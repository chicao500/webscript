import requests
from bs4 import BeautifulSoup

req = requests.get('https://www.cepea.esalq.usp.br/br/indicador/milho.aspx')

soup = BeautifulSoup (req.content, 'html.parser')

item = soup.select('table tbody tr td')


cont = 1
print ('\nDATA          VALOR R$  VAR/DIA  VAR/MÊS  VALOR(U$)\n')
for i in range(len(item)):
	
	if cont is not 5:
		print (item[i].text, end = '     ')
	else:
		print (item[i].text)
		cont = 0

	cont = cont + 1