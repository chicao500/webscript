import requests
from bs4 import BeautifulSoup

def grava_arquivo():
	arq.write('TIME ; PONTOS ; JOGOS ; VITÓRIAS ; EMPATES ; DERROTAS ; GOLS PRÓ ; GOLS CONTRA ; SALDO/GOLS ; APROVEITAMENTO(%) \n')
	for i in range(len(time)):
		aux = time[i].text+ ' ; '+  points[i].text+ ' ; '+jogos[i].text+ ' ; '+vitorias[i].text+' ; '+empates[i].text+' ; '+derrotas[i].text+' ; '+golspro[i].text+ ' ; '+golscontra[i].text+' ; '+saldo[i].text+ ' ; '+aproveitamento[i].text+ '\n'
		arq.write(aux)

def exibir_tabela():
	for i in range(len(time)):
		print ('=========[ '+time[i].text+' ]=========')
		print ('Pontos: ' +points[i].text)
		print ('Jogos: '+jogos[i].text)
		print ('Vitórias: '+vitorias[i].text)
		print ('Empates: '+empates[i].text)
		print ('Derrotas: '+derrotas[i].text)
		print ('Gols Pró: '+golspro[i].text)
		print ('Gols Contra: '+golscontra[i].text)
		print ('Saldo de Gols: '+saldo[i].text)
		print ('Aproveitamento(%): '+aproveitamento[i].text)
		print ('\n')
	

with open ('table_laliga.csv', 'w') as arq:

	req = requests.get ('https://www.terra.com.br/esportes/futebol/internacional/espanha/campeonato-espanhol/tabela/')

	soup = BeautifulSoup(req.content, 'html.parser')

	time = soup.select('tbody tr  td.main.team-name ')
	points = soup.select ('tbody tr td.points')
	jogos = soup.select ('tbody tr td[title|=Jogos]')
	vitorias = soup.select ('tbody tr td[title|=Vitórias]')
	empates = soup.select ('tbody tr td[title|=Empates]')
	derrotas = soup.select ('tbody tr td[title|=Derrotas]')
	golspro = soup.find_all('td', title='Gols Pró')
	golscontra = soup.find_all('td', title='Gols Contra')
	saldo = soup.find_all('td', title='Saldo de Gols')
	aproveitamento = soup.select ('tbody tr td[title|=Aproveitamento]')



	exibir_tabela()
	grava_arquivo()