import requests
from bs4 import BeautifulSoup

with open ('precomilho.csv', 'w+') as arq:

	req = requests.get ('https://www.noticiasagricolas.com.br/cotacoes/milho/indicador-cepea-esalq-milho')

	soup = BeautifulSoup(req.content, "html.parser")

	produto = soup.select('table.cot-fisicas tbody tr td')

	cont = 1

	print ("\nCotação do milho nos ultimos dias (Valor R$/ Saca de 60 kg)\n\n")
	cabecalho = 'DATA | PREÇO (R$) | VARIAÇÃO (%)'
	arq.write (cabecalho + '\n')


	for link in produto:
		
		if cont == 1:
			print ("Data: " + link.text)
			arq.write(str(link.text).replace(',','.') + ' | ')

		if cont == 2:
			print ("Preço: R$ "+ link.text)
			arq.write(str(link.text).replace(',','.') + ' | ')
			
		if cont == 3:
			print ("Variação: "+link.text + "%"+"\n"+'-'*20)
			arq.write(str(link.text).replace(',','.') + ' | ')
			arq.write ('\n')
			cont = 0


		cont = cont + 1

	print ('\nArquivo CSV gerado com sucesso !!!\n')