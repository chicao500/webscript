import requests
from bs4 import BeautifulSoup


invocador = input('Digite o nome de invocador: ')
nick = open ('HistoricosOPGG/HistoricoDe'+ str(invocador) +'.csv', 'w')


req = requests.get ('http://br.op.gg/summoner/userName='+ str(invocador))
soup = BeautifulSoup (req.content, "html.parser")

kill = soup.select('div.GameItemList div.KDA span.Kill')
death = soup.select ('div.GameItemList div.KDA span.Death')
assist = soup.select ('div.GameItemList div.KDA span.Assist')
nome_campeao = soup.select ('div.GameItemList div.ChampionName')
tier =  soup.find("span", {"class": "tierRank"})
resultado_game = soup.findAll("div", {'class': "GameResult"})
tipo_game = soup.findAll ("div", {"class": "GameType"})


print ('\n' + '='*20 +'[ '+ tier.text+' ]' + '='*20 + '\n')

exibirnick = 'Invocador | '+ str(invocador)
cabecalho = 'KDA | Campeão | Resultado | Tipo'


nick.write (exibirnick + '\n\n')
nick.write (cabecalho + '\n')

for i in range(len(nome_campeao)):
	if 'Kill' not in kill[i].text:
		print (kill[i].text+'/'+death[i].text+'/'+assist[i].text+nome_campeao[i].text+resultado_game[i].text.strip()+'\n'+tipo_game[i].text.strip(),"\n")

		kda = str(kill[i].text.strip()) + '/'+str(death[i].text.strip()) + '/'+ str(assist[i].text.strip())
		aux = kda + ' | '+ nome_campeao[i].text.strip() + ' | ' + resultado_game[i].text.strip().replace('Victory','VITÓRIA').replace('Defeat','DERROTA')+ ' | '+ tipo_game[i].text.strip() + '\n'
		
		nick.write (aux)
	

nick.close()